configs = {

    'ga_dj': {
        'population_size': 50,
        'offspring_population_size' : 50,
        'mutation' : 0.30,
        'crossover' : 0.50,
        'max_evaluations' : 100000
    },

    'sa_dj': {
        'mutation' : 0.50,
        'max_evaluations' : 100000
    },

    'aco_dj': {
        'ant_count' : 10,
        'generations' : 100,
        'alpha' : 1.0,
        'beta' : 2.0,
        'rho' : 0.6,
        'q' : 10,
        'strategy' : 2
    },

    'ga_qa': {
        'population_size' : 50,
        'offspring_population_size' : 32,
        'mutation' : 0.70,
        'crossover' : 0.90,
        'max_evaluations' : 100000
    },

    'sa_qa': {
        'mutation' : 0.85,
        'max_evaluations' : 100000
    },

    'aco_qa': {
        'ant_count' : 10,
        'generations' : 100,
        'alpha' : 1.0,
        'beta' : 6.0,
        'rho' : 0.6,
        'q' : 10,
        'strategy' : 2
    }

}