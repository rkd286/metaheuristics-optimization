from src.discrete.genetic_algorithm import TSPGeneticAlgorithm
from src.discrete.simulated_annealing import TSPSimulatedAnnealing
from src.discrete.aco import TSPAntColony
from src.discrete.default_config import configs


def tsp(algo='aco', city='dj', **kwargs):
    
    if city == 'dj':
        tsp_file = 'dj38.tsp'
    elif city == 'qa':
        tsp_file = 'qa194.tsp'

    config = configs[algo + '_' + city]

    if algo == 'ga':

        for key in list(set(config.keys()) - set(kwargs.keys())):
            kwargs[key] = config[key]
        
        ga = TSPGeneticAlgorithm(
            tsp_file = tsp_file,
            population_size = kwargs['population_size'],
            offspring_population_size = kwargs['offspring_population_size'],
            mutation = kwargs['mutation'],
            crossover = kwargs['crossover'],
            max_evaluations = kwargs['max_evaluations']
        )

        ga.optimize()
    
    elif algo == 'sa':

        for key in list(set(config.keys()) - set(kwargs.keys())):
            kwargs[key] = config[key]
        
        sa = TSPSimulatedAnnealing(
            tsp_file = tsp_file,
            mutation = kwargs['mutation'],
            max_evaluations = kwargs['max_evaluations']
        )

        sa.optimize()
    
    elif algo == 'aco':

        for key in list(set(config.keys()) - set(kwargs.keys())):
            kwargs[key] = config[key]
        
        aco = TSPAntColony(
            tsp_file = tsp_file,
            ant_count = kwargs['ant_count'],
            generations = kwargs['generations'],
            alpha = kwargs['alpha'],
            beta = kwargs['beta'],
            rho = kwargs['rho'],
            q = kwargs['q'],
            strategy = kwargs['strategy']
        )

        aco.optimize()
