from pygmo import algorithm, algorithms, population
import pygmo as pg
from src.continuous.default_config import fitness, dims
from util.plot import loss_plot
from util.stopwatch import tick_tock


class CMAES:
    def __init__(self, fn_name, function, population_size, generations):
        self.fn_name = fn_name
        self.function = function
        self.population_size = population_size
        self.generations = generations
    
    def optimize(self):
        
        clock = tick_tock()
        clock.start_time()


        cma_algo = algorithm(algorithms.cmaes(gen = self.generations))
        cma_algo.set_verbosity(50)
        cma_prob = pg.problem(self.function)
        cma_pop = population(cma_prob, self.population_size)
        cma_pop = cma_algo.evolve(cma_pop)
        print("Fitness: {}".format(cma_pop.champion_f))
        print("Solution: {}".format(cma_pop.champion_x))
        print("Summary: {}".format(cma_pop))

        clock.end_time()
        clock.total_time()

        loss_plot(self.population_size, fitness, self.fn_name + '_' +  str(dims))
