from src.continuous.functions import functions_dict
from src.continuous.default_config import configs
from src.continuous import default_config
from src.continuous.particle_swarm_optimization import PSO
from src.continuous.cmaes import CMAES


def continuous(fn_name='sphere', algo='pso', dims=50, **kwargs):
    
    default_config.dims = dims
    config = configs[fn_name + '_' + algo + '_' + str(dims)]
    
    for key in list(set(config.keys()) - set(kwargs.keys())):
            kwargs[key] = config[key]

    if algo == 'pso':
        pso = PSO(
            fn_name=fn_name,
            function=functions_dict[fn_name](),
            population_size=kwargs['population_size'],
            generations = kwargs['generations'],
            omega = kwargs['omega'],
            eta1 = kwargs['eta1'],
            eta2 = kwargs['eta2']
        )
        
        pso.optimize()
        
    elif algo == 'cma':
        function = functions_dict[fn_name]()
        cma = CMAES(
            fn_name=fn_name,
            function=function,
            population_size=kwargs['population_size'],
            generations = kwargs['generations']
        )

        cma.optimize()
        
