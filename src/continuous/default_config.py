dims = 0
fitness = []

configs = {
    ##############################################  PSO 50  ###############################################

    'sphere_pso_50': 
    {
        'population_size': 50,
        'generations': 5000,
        'omega': 0.72,
        'eta1': 2.1,
        'eta2': 2.8
    },

    'schwefel_pso_50': 
    {
        'population_size': 50,
        'generations': 5000,
        'omega': 0.72,
        'eta1': 2.2,
        'eta2': 2.5
    },

    'rosenbrock_pso_50': 
    {
        'population_size': 50,
        'generations': 10000,
        'omega': 0.72,
        'eta1': 1.8,
        'eta2': 2.05
    },

    'rastrigin_pso_50': 
    {
        'population_size': 500,
        'generations': 10000,
        'omega': 0.7298,
        'eta1': 2.30,
        'eta2': 2.05
    },

    'griewank_pso_50': 
    {
        'population_size': 500,
        'generations': 10000,
        'omega': 0.7298,
        'eta1': 2.1,
        'eta2': 2.8
    },

    'ackley_pso_50': 
    {
        'population_size': 50,
        'generations': 10000,
        'omega': 0.72,
        'eta1': 2.1,
        'eta2': 2.8
    },

    ##############################################  PSO 500  ###############################################

    'sphere_pso_500': 
    {
        'population_size': 50,
        'generations': 5000,
        'omega': 0.72,
        'eta1': 2.1,
        'eta2': 2.8
    },

    'schwefel_pso_500': 
    {
        'population_size': 50,
        'generations': 5000,
        'omega': 0.72,
        'eta1': 2.2,
        'eta2': 2.5
    },

    'rosenbrock_pso_500': 
    {
        'population_size': 50,
        'generations': 10000,
        'omega': 0.72,
        'eta1': 1.8,
        'eta2': 2.05
    },

    'rastrigin_pso_500': 
    {
        'population_size': 500,
        'generations': 10000,
        'omega': 0.7298,
        'eta1': 2.30,
        'eta2': 2.05
    },

    'griewank_pso_500': 
    {
        'population_size': 500,
        'generations': 10000,
        'omega': 0.7298,
        'eta1': 2.1,
        'eta2': 2.8
    },

    'ackley_pso_500': 
    {
        'population_size': 50,
        'generations': 10000,
        'omega': 0.72,
        'eta1': 2.1,
        'eta2': 2.8
    },

    ##############################################  CMA 50  ###############################################

    'sphere_cma_50': 
    {
        'population_size': 50,
        'generations': 500,
    },

    'schwefel_cma_50': 
    {
        'population_size': 50,
        'generations': 300,
    },

    'rosenbrock_cma_50': 
    {
        'population_size': 50,
        'generations': 5000,
    },

    'rastrigin_cma_50': 
    {
        'population_size': 500,
        'generations': 1000,
    },

    'griewank_cma_50': 
    {
        'population_size': 50,
        'generations': 500,
    },

    'ackley_cma_50': 
    {
        'population_size': 500,
        'generations': 5000,
    },

    ##############################################  CMA 500  ###############################################

    'sphere_cma_500': 
    {
        'population_size': 50,
        'generations': 500,
    },

    'schwefel_cma_500': 
    {
        'population_size': 50,
        'generations': 300,
    },

    'rosenbrock_cma_500': 
    {
        'population_size': 50,
        'generations': 5000,
    },

    'rastrigin_cma_500': 
    {
        'population_size': 500,
        'generations': 1000,
    },

    'griewank_cma_500': 
    {
        'population_size': 50,
        'generations': 500,
    },

    'ackley_cma_500': 
    {
        'population_size': 500,
        'generations': 1000,
    },

}