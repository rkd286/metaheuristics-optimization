from pygmo import algorithm, algorithms, population
import pygmo as pg
from src.continuous.default_config import fitness, dims
from util.plot import loss_plot
from util.stopwatch import tick_tock


class PSO:
    def __init__(self, fn_name, function, population_size, generations, omega, eta1, eta2):
        self.fn_name = fn_name
        self.function = function
        self.population_size = population_size
        self.generations = generations
        self.omega = omega
        self.eta1 = eta1
        self.eta2 = eta2
    
    def optimize(self):
        clock = tick_tock()
        clock.start_time()
        
        pso_algo = algorithm(algorithms.pso(gen = self.generations,
                                            omega=self.omega,
                                            eta1=self.eta1,
        
                                            eta2=self.eta2))
        pso_algo.set_verbosity(50)
        pso_prob = pg.problem(self.function)
        pso_pop = population(pso_prob, self.population_size)
        pso_pop = pso_algo.evolve(pso_pop)
        print("Fitness: {}".format(pso_pop.champion_f))
        print("Solution: {}".format(pso_pop.champion_x))
        print("Summary: {}".format(pso_pop))
        
        clock.end_time()
        clock.total_time()

        loss_plot(self.population_size, fitness, self.fn_name + '_' +  str(dims))
