import numpy as np
from input import continuous_data
from src.continuous import default_config
from src.continuous.default_config import fitness


class shifted_sphere:
        
    def fitness(self, x):
        bias = -450.0
        dims = default_config.dims
        F = np.sum(np.square(np.array(x) - np.array(continuous_data.sphere[:dims])))
        val = F + bias
        fitness.append(val)
        return [val, ]
    
    def get_bounds(self):
        dims = default_config.dims
        return ([-100]*dims,[100]*dims)


class schwefel_problem:
    
    def fitness(self, x):
        dims = default_config.dims
        bias = -450.0
        z = np.array(x) - np.array(continuous_data.schwefel[:dims])
        F = np.max(np.abs(z))
        val = F + bias
        fitness.append(val)
        return [val, ]
    
    def get_bounds(self):
        dims = default_config.dims
        return ([-100]*dims,[100]*dims)


class shifted_rosenbrock:
    
    def fitness(self, x):
        dims = default_config.dims
        bias = 390.0
        F = 0.0
        z = np.array(x) - np.array(continuous_data.rosenbrock[:dims]) + 1
        
        for i in range(dims-1):
            F += (100 * (z[i]**2 - z[i+1])**2) + (z[i] - 1)**2
        val = F + bias
        fitness.append(val)
        return [val, ]
    
    def get_bounds(self):
        dims = default_config.dims
        return ([-100]*dims,[100]*dims)


class shifted_rastrigin:
    
    def fitness(self, x):
        dims = default_config.dims
        bias = -330.0
        z = np.array(x) - np.array(continuous_data.rastrigin[:dims])
        F = np.sum(np.square(z) - (10*np.cos(2*np.pi*z)) + 10)
        val = F + bias
        fitness.append(val)
        return [val, ]
    
    def get_bounds(self):
        dims = default_config.dims
        return ([-5]*dims,[5]*dims)


class shifted_griewank:
    
    def fitness(self, x):
        dims = default_config.dims
        bias = -180.0
        z = np.array(x) - np.array(continuous_data.griewank[:dims])
        F1 = np.sum(np.square(z) / 4000.0)
        F2 = np.prod(np.cos(z/np.sqrt((np.arange(dims) + 1))))
        val = F1 - F2 + 1 + bias
        fitness.append(val)
        return [val, ]
    
    def get_bounds(self):
        dims = default_config.dims
        return ([-600]*dims,[600]*dims)


class shifted_ackley:

    
    def fitness(self, x):
        dims = default_config.dims
        bias = -140.0
        z = np.array(x) - np.array(continuous_data.ackley[:dims])
        S1 = np.sqrt(np.sum(np.square(z)) / dims)
        S2 = np.exp(np.sum(np.cos(2*np.pi*z)) / dims)
        val = (-20*np.exp(-0.2*S1-S2)) + 20 + np.exp(1) + bias
        fitness.append(val)
        return [val, ]
    
    
    def get_bounds(self):
        dims = default_config.dims
        return ([-32]*dims,[32]*dims)


functions_dict = {
    'sphere': shifted_sphere,
    'schwefel': schwefel_problem,
    'rosenbrock': shifted_rosenbrock,
    'rastrigin': shifted_rastrigin,
    'griewank': shifted_griewank,
    'ackley': shifted_ackley
}