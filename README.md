# Metaheuristics Optimization

## Objective
To optimize the following discrete and continuous problems using metaheuristics

### Traveling Salesman Problem:

    Djibouti - 38 Cities
    Qatar - 194 Cities

### Continuous Functions:

    F1: Shifted Sphere Function
    F2: Shifted Schwefel’s Function
    F3: Shifted Rosenbrock’s Function
    F4: Shifted Rastrigin's Function
    F5: Shifted Griewank's Function
    F6: Shifted Ackley Function

## Setup

### Tools Used:

* Python 3.7.1
* Libraries:
    * JmetalPy with Custom TSP Class to accomodate Float values
    * Custom Ant Colonly Optimization Class
    * pygmo 2.0
    * tsplib95
    * numpy
    * matplotlib
    * fire


### Requirements:

* Python 3.6+

### Clone and Install

```
git clone https://gitlab.com/rkd286/metaheuristics-optimization.git
```

```
cd metaheuristics-optimization
```

```
pip3 install -r requirements.txt
```

> **Note: The commands to run each algorithm is attached inline**


## Discrete Optimization - The Traveling Salesman Problem
The following 3 algorithms were used to optimize the problem:

* Genetic Algorithm (GA)
* Simulated Annealing (SA)
* Ant Colony Optimization (ACO)

### Best Algorithm: Ant Colony Optimization

|   City   |                                                  Parameters                                                 |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |              Solution             |
|:--------:|:-----------------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:---------------------------------:|
| Djibouti |  Ant Count: 10<br>     Generations: 100<br>     Alpha: 1.0<br>     Beta: 6.0<br>     Rho: 0.6<br>     Q: 10 | 6668.287 |         1000         | Stop by Max Evaluations |        6.43s       | [See Solution](./output/discrete/aco_dj38.txt) |
|   Qatar  | Ant Count: 10<br>     Generations: 100<br>     Alpha: 1.0<br>     Beta: 12.0<br>     Rho: 0.6<br>     Q: 10 |  10696.2 |         1000         | Stop by Max Evaluations |       1m 44s       |  [See Solution](./output/discrete/aco_qa194.txt)  |


### Parameters:

**Alpha:** Relative importance of Pheromone<br>
**Beta:** Relative importance of Heuristic Information<br>
**Rho:** Evaporation Rate<br>
**Q:** Pheromone Intensity

### Justification:

Dorian Gaertner and Keith Clark in ```[1]``` yielded 1,386 different parameter combinations and ran the algorithm 10 times on each of these combinations. This exhaustive search helped them find the best configurations.
<br><br>
The idea was to choose a higher beta value to increase the visibility of the edge node and to keep a low alpha value to decrease the trail. The paper suggests to fix alpha values and only vary beta. As noticed, the beta value is higher for Qatar since it has more cities.
<br><br>
The evaporation factor rho ensures that pheromone is not accumulated infinitely and denotes the proportion of 'old' pheromone that is carried over to the next iteration of the algorithm.


### Optimal Path

### 1. Djibouti

![Djibouti](./img/discrete/aco_dj38.png)

### 2. Qatar

![Djibouti](./img/discrete/aco_qa194.png)

### Usage

```python
python optimize.py tsp --algo=aco --city=dj
```

>**Parameters (Optional)**  
>--city -> [dj, qa]  
>--ant_count  
>--generations  
>--alpha  
>--beta  
>--rho  
>--q  
>--strategy -> [0: ant cylce, 1: ant quality, 2: ant density]  

### Comparison with other Algorithms:

### 1) Genetic Algorithm

|   City   |                                                                  Parameters                                                                  |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                  Solution                 |
|:--------:|:--------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:-----------------------------------------:|
| Djibouti | Population: 50<br>     Offspring Population Size: 50<br>     Mutation: 0.30<br>     Crossover: 0.50<br>     Max Evaluations: 100000<br>      | 10387.38 |        100000        | Stop by Max Evaluations |        6.59s       | [See Solution](./output/discrete/ga_dj38.txt) |
|   Qatar  | Population: 50<br>     Offspring Population Size: 32<br>     Mutation: 0.70<br>     Crossover: 0.90<br>     Max Evaluations: 100000<br>      | 20035.73 |        100000        | Stop by Max Evaluations |       1m 14s       |  [See Solution](./output/discrete/ga_qa194.txt) |

### Justification:
We use Swap Mutation where, we select two positions on the chromosome at random, and interchange the values. This is common in permutation based encodings.
<br><br>
The Partially Mapped Crossover (PMX) chooses two random cut points on parents to build offspring, the portion between cut points, one parent’s string is mapped onto the other parent’s string and the remaining information is exchanged.
<br><br>
The parameters were chosen based on ```[2]```

### Usage

```
python optimize.py tsp --algo=ga --city=dj
```

>**Parameters (Optional)**  
>--city -> [dj, qa]  
>--population_size  
>--offspring_population_size  
>--mutation  
>--crossover  
>--max_evaluations  


### 2) Simulated Annealing

|   City   |                                              Parameters                                              |  Fitness  | Function Evaluations |    Stopping Criterion   | Computational Time |                     Solution                    |
|:--------:|:----------------------------------------------------------------------------------------------------:|:---------:|:--------------------:|:-----------------------:|:------------------:|:-----------------------------------------------:|
| Djibouti | Temperature: 100<br>     Alpha: 0.95<br>     Mutation: 0.50<br>     Max Evaluations: 100000<br>      | 10029.613 |        100000        | Stop by Max Evaluations |        5.23s       | [See   Solution](./output/discrete/sa_dj38.txt) |
|   Qatar  | Temperature: 100<br>     Alpha: 0.95<br>     Mutation: 0.85<br>     Max Evaluations: 100000<br>      | 18945.568 |        100000        | Stop by Max Evaluations |         18s        |  [See   Solution](./output/discrete/sa_qa194.txt) |

### Justification:
The parameters were chosen based on [3]
<br><br>
The idea is to set an initial high temperature followed by gradual cooling. The temperature and cooling rate as per ```[3]``` were set higher than the default values - 100 and 0.95 respectively
<br><br>

### Usage

```
python optimize.py tsp --algo=sa --city=dj
```

>**Parameters (Optional)**  
>--city -> [dj, qa]  
>--mutation  
>--crossover  

### Conclusion
ACO seems to perform better compared to Genetic Algorithm and Simulated Annealing as evidenced in ```[3]``` that makes a comparative study of the 3 algorithms. When it comes to execution time, Simulated Annealing is the quickest. Moreover, we can also see that Simulated Annealing performs better than Genetic Algorithm. This is backed by ```[4]```. The Selection Algorithm chosen for GA is Tournament Selection, which is naturally elitist as recommended in ```[5]```.

***

## Continuous Optimization
The following 2 algorithms were used to optimize the problem:

* Particle Swarm Optimization (PSO)
* Covariance Matrix Adaption - Evolution Strategy (CMA-ES)

### F1: Shifted Sphere
### Best Algorithm: CMA-ES

| Dimension |                 Parameters                |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                         Solution                         |
|:---------:|:-----------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:--------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 500  | -449.661 |        250050        | Stop by Max Evaluations |         4s         |  [See   Solution](./output/continuous/sphere_cma_50.txt) |
|    500    | Population: 200<br>     Generations: 1000 | -449.999 |        170400        | Stop by Max Evaluations |      1h 20m 6s     | [See   Solution](./output/continuous/sphere_cma_500.txt) |


### Convergence Curve
### Dimension: 50
![Shifted Sphere](./img/continuous/sphere_cma_50.png)

### Dimension: 500
![Shifted Sphere](./img/continuous/sphere_cma_500.png)

### Usage

```
python optimize.py continuous --fn_name=sphere --algo=cma --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations

### F2: Shifted Schwefel’s
### Best Algorithm: CMA-ES

| Dimension |                 Parameters                |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                          Solution                          |
|:---------:|:-----------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:----------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 300  | -449.972 |         15050        | Stop by Max Evaluations |        874ms       |  [See   Solution](./output/continuous/schwefel_cma_50.txt) |
|    500    | Population: 200<br>     Generations: 1000 | -448.515 |        200200        | Stop by Max Evaluations |     1h 32m 59s     | [See   Solution](./output/continuous/schwefel_cma_500.txt) |

### Convergence Curve
### Dimension: 50
![Shifted Schwefel](./img/continuous/schwefel_cma_50.png)

### Dimension: 500
![Shifted Schwefel](./img/continuous/schwefel_cma_500.png)

### Usage

```
python optimize.py continuous --fn_name=schwefel --algo=cma --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations

### F3: Shifted Rosenbrock
### Best Algorithm: CMA-ES

| Dimension |                 Parameters                 | Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                           Solution                          |
|:---------:|:------------------------------------------:|:-------:|:--------------------:|:-----------------------:|:------------------:|:-----------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 5000  | 390.001 |        193300        | Stop by Max Evaluations |        29.3s       |  [See   Solution](./output/continuous/rosenbrock_cma_50.txt) |
|    500    | Population: 200<br>     Generations: 10000 | 970.758 |        1000100       | Stop by Max Evaluations |      8h 31m 2s     | [See   Solution](./output/continuous/rosenbrock_cma_500.txt) |

### Convergence Curve
### Dimension: 50
![Shifted Rosenbrock](./img/continuous/rosenbrock_cma_50.png)

### Dimension: 500
![Shifted Rosenbrock](./img/continuous/rosenbrock_cma_500.png)

### Usage

```
python optimize.py continuous --fn_name=rosenbrock --algo=cma --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations

### F4: Shifted Rastrigin
### Best Algorithm: CMA-ES

| Dimension |                 Parameters                 | Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                           Solution                           |
|:---------:|:------------------------------------------:|:-------:|:--------------------:|:-----------------------:|:------------------:|:------------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 1000  | -328.01 |        270500        | Stop by Max Evaluations |        14.1s       |  [See   Solution](./output/continuous/rastrigin_cma_50.txt) |
|    500    | Population: 100<br>     Generations: 10000 | 186.383 |        152000        | Stop by Max Evaluations |     1h 13m 59s     | [See   Solution](./output/continuous/rastrigin_cma_500.txt) |


### Convergence Curve
### Dimension: 50
![Shifted Rastrigin](./img/continuous/rastrigin_cma_50.png)

### Dimension: 500
![Shifted Rastrigin](./img/continuous/rastrigin_cma_500.png)

### Usage

```
python optimize.py continuous --fn_name=rastrigin --algo=cma --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations

### F5: Shifted Griewank
### Best Algorithm: CMA-ES

| Dimension |                Parameters                |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                          Solution                          |
|:---------:|:----------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:----------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 500 | -179.999 |         18700        | Stop by Max Evaluations |        1.39s       |  [See   Solution](./output/continuous/griewank_cma_50.txt) |
|    500    | Population: 100<br>     Generations: 500 | -179.079 |         50100        | Stop by Max Evaluations |       25m 9s       | [See   Solution](./output/continuous/griewank_cma_500.txt) |

### Convergence Curve
### Dimension: 50
![Shifted Griewank](./img/continuous/griewank_cma_50.png)

### Dimension: 500
![Shifted Griewank](./img/continuous/griewank_cma_500.png)

### Usage

```
python optimize.py continuous --fn_name=griewank --algo=cma --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations

### F6: Shifted Ackley
### Best Algorithm: CMA-ES

| Dimension |                 Parameters                |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                         Solution                         |
|:---------:|:-----------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:--------------------------------------------------------:|
|     50    | Population: 500<br>     Generations: 5000 | -128.825 |        234000        | Stop by Max Evaluations |        14.4s       |  [See   Solution](./output/continuous/ackley_cma_50.txt) |
|    500    | Population: 200<br>     Generations: 5000 | -129.354 |        240200        | Stop by Max Evaluations |     1h 53m 16s     | [See   Solution](./output/continuous/ackley_cma_500.txt) |

### Convergence Curve
### Dimension: 50
![Shifted Ackley](./img/continuous/ackley_cma_50.png)

### Dimension: 500
![Shifted Ackley](./img/continuous/ackley_cma_500.png)


### Usage

```
python optimize.py continuous --fn_name=ackley --algo=cma --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations

### Justification

The Covariance Matrix Adaptation Evolution Strategy (CMA-ES) is widely accepted as a robust derivative-free continuous optimization algorithm for non-linear and non-convex optimization problems.
<br><br>

According to Ilya Loshchilov, Marc Schoenauer, Michèle Sebag, Nikolaus Hansen in ```[6]```, it is suggested to not change any parameters other than population size in CMA-ES. Other hyperparameters have been provided robust default settings, in the sense that their offline tuning allegedly hardly improves the CMA-ES performance for unimodal functions. Hence, the CMA-ES  algorithm is well known to be almost parameterless. On the downside, we have increase computation time, especially for complex functions with higher dimensions.


### Comparison with Particle Swarm Optimization
### F1: Shifted Sphere

| Dimension |                                             Parameters                                             |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                         Solution                         |
|:---------:|:--------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:--------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 5000<br>     Omega: 0.72<br>     Eta1: 2.1<br>     Eta2: 2.8  | -449.661 |        250050        | Stop by Max Evaluations |         4s         |  [See   Solution](./output/continuous/sphere_pso_50.txt) |
|    500    | Population: 500<br>     Generations: 10000<br>     Omega: 0.72<br>     Eta1: 2.1<br>     Eta2: 2.8 | 84673.87 |        5000500       | Stop by Max Evaluations |       4m 13s       | [See   Solution](./output/continuous/sphere_pso_500.txt) |

### Usage

```
python optimize.py continuous --fn_name=sphere --algo=pso --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations
>--omega
>--eta1
>--eta2

### F2: Shifted Schwefel

| Dimension |                                             Parameters                                             |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                          Solution                          |
|:---------:|:--------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:----------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 5000<br>     Omega: 0.72<br>     Eta1: 2.2<br>     Eta2: 2.5  | -434.538 |        250050        | Stop by Max Evaluations |        3.84s       |  [See   Solution](./output/continuous/schwefel_pso_50.txt) |
|    500    | Population: 200<br>     Generations: 5000<br>     Omega: 0.72<br>     Eta1: 2.05<br>     Eta2: 2.1 | -333.996 |        1000200       | Stop by Max Evaluations |        1m 3s       | [See   Solution](./output/continuous/schwefel_pso_500.txt) |

### Usage

```
python optimize.py continuous --fn_name=schwefel --algo=pso --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations
>--omega
>--eta1
>--eta2

### F3: Shifted Rosenbrock

| Dimension |                                              Parameters                                             |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                           Solution                           |
|:---------:|:---------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:------------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 10000<br>     Omega: 0.72<br>     Eta1: 1.8<br>     Eta2: 2.05 |  419.113 |        500050        | Stop by Max Evaluations |        56.7s       |  [See   Solution](./output/continuous/rosenbrock_pso_50.txt) |
|    500    | Population: 500<br>     Generations: 10000<br>     Omega: 0.72<br>     Eta1: 1.8<br>     Eta2: 2.05 | 5322.993 |        5000500       | Stop by Max Evaluations |     1h 29m 37s     | [See   Solution](./output/continuous/rosenbrock_pso_500.txt) |

### Usage

```
python optimize.py continuous --fn_name=rosenbrock --algo=pso --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations
>--omega
>--eta1
>--eta2

### F4: Shifted Rastrigin

| Dimension |                                               Parameters                                               |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                           Solution                          |
|:---------:|:------------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:-----------------------------------------------------------:|
|     50    | Population: 500<br>     Generations: 10000<br>     Omega: 0.7298<br>     Eta1: 2.30<br>     Eta2: 2.05 |  -249.37 |        5000500       | Stop by Max Evaluations |       1m 47s       |  [See   Solution](./output/continuous/rastrigin_pso_50.txt) |
|    500    |  Population: 500<br>     Generations: 20000<br>     Omega: 0.72<br>     Eta1: 2.30<br>     Eta2: 2.05  | 2104.253 |       10000500       | Stop by Max Evaluations |       8m 45s       | [See   Solution](./output/continuous/rastrigin_pso_500.txt) |

### Usage

```
python optimize.py continuous --fn_name=rastrigin --algo=pso --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations
>--omega
>--eta1
>--eta2

### F5: Shifted Griewank

| Dimension |                                              Parameters                                              |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                          Solution                          |
|:---------:|:----------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:----------------------------------------------------------:|
|     50    |  Population: 50<br>     Generations: 5000<br>     Omega: 0.7298<br>     Eta1: 2.1<br>     Eta2: 2.8  | -179.837 |        250050        | Stop by Max Evaluations |        6.89s       |  [See   Solution](./output/continuous/griewank_pso_50.txt) |
|    500    | Population: 200<br>     Generations: 10000<br>     Omega: 0.7298<br>     Eta1: 2.1<br>     Eta2: 2.8 | 1394.289 |         50100        | Stop by Max Evaluations |       25m 9s       | [See   Solution](./output/continuous/griewank_pso_500.txt) |

### Usage

```
python optimize.py continuous --fn_name=griewank --algo=pso --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations
>--omega
>--eta1
>--eta2

### F6: Shifted Ackley

| Dimension |                                             Parameters                                             |  Fitness | Function Evaluations |    Stopping Criterion   | Computational Time |                         Solution                         |
|:---------:|:--------------------------------------------------------------------------------------------------:|:--------:|:--------------------:|:-----------------------:|:------------------:|:--------------------------------------------------------:|
|     50    | Population: 200<br>     Generations: 10000<br>     Omega: 0.72<br>     Eta1: 2.1<br>     Eta2: 2.8 | -128.341 |        2000200       | Stop by Max Evaluations |        58.5s       |  [See   Solution](./output/continuous/ackley_pso_50.txt) |
|    500    | Population: 100<br>     Generations: 10000<br>     Omega: 0.72<br>     Eta1: 2.1<br>     Eta2: 2.8 | -119.039 |        1000100       | Stop by Max Evaluations |        1m 3s       | [See   Solution](./output/continuous/ackley_pso_500.txt) |

### Usage

```
python optimize.py continuous --fn_name=ackley --algo=pso --dims=50
```

>**Parameters (Optional)**  
>--dims -> [50, 500]  
>--population_size  
>--generations
>--omega
>--eta1
>--eta2

### Justification
Eta1 and Eta2 are acceleration constants a.k.a, cognition and social components. These parameters control the personal movement/velocity and global movement/velocity.
<br><br>

Small cognition component implies more diversification and large social component means more diversification. Functions with a lot of crests and troughs are subjected to more diversification and on the other hand, functions with not many crests and troughs have larger intensification as they are likely to converge quickly.

The parameters were tuned based on ```[7]```.

### Conclusion
It seems that CMA-ES in every case works/converges better compared to PSO. The global minima wasn't achieved in some cases, probably due to hardware restrictions (execution time). 

Pawel Szynkiewicz in ```[8]``` proved that CMA-ES is always preferred over PSO. Moreover, CMA-ES being almost 'hyperparameter free' compared to PSO, where considerable amount of tuning is required.


## References
1. [Optimal Parameters for Ant Colony Optimization algorithms - Dorian Gaertner and Keith Clark](https://www.researchgate.net/publication/220835272_On_Optimal_Parameters_for_Ant_Colony_Optimization_Algorithms)

2. [Introduction to Genetic Algorithms](https://www.obitko.com/tutorials/genetic-algorithms/example-function-minimum.php)

3. [Performance Comparison of Simulated Annealing GA ACO Applied to TSP - Hosam Mukhairez](https://www.academia.edu/27378990/Performance_Comparison_of_Simulated_Annealing_GA_ACO_Applied_to_TSP)

4. [The comparison between Simulated Annealing Algorithm and Genetic Algorithm in order to solve Traveling Salesman Problem in Isfahan telecommunications companies - Ali Karevan1, Seyed Mahdi Homayouni, Arian Hesami, Sepideh MohamadRezaie Larki](https://www.researchgate.net/publication/327689888_The_comparison_between_Simulated_Annealing_Algorithm_and_Genetic_Algorithm_in_order_to_solve_Traveling_Salesman_Problem_in_Esfahan_telecommunications_companies)

5. [Genetic Algorithms and the Traveling Salesman Problem - Konstantin Boukreev](https://www.codeproject.com/Articles/1403/Genetic-Algorithms-and-the-Traveling-Salesman-Prob)

6. [Maximum Likelihood-based Online Adaptation of Hyper-parameters in CMA-ES - Ilya Loshchilov, Marc Schoenauer, Michèle Sebag, Nikolaus Hansen](https://hal.inria.fr/hal-01003504v2/document)

7. [The Parameters Selection of PSO Algorithm influencing On performance of Fault Diagnosis - Yan HE, Wei Jin MA, Ji Ping ZHANG](https://www.matec-conferences.org/articles/matecconf/pdf/2016/26/matecconf_mmme2016_02019.pdf)

8. [A Comparative Study of PSO and CMA-ES Algorithms on Black-box Optimization Benchmarks - Pawel Szynkiewicz](https://www.il-pib.pl/czasopisma/JTIT/2018/4/5.pdf)