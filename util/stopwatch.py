import time

class tick_tock:
    
    def __init__(self):
        self.total = 0.0
        self.start = 0
        self.end = 0
    
    def start_time(self):
        self.start = time.time()
    
    def end_time(self):
        self.end = time.time()
    
    def total_time(self):
        self.total = self.end - self.start
        print("Computing Time: {} seconds".format(round(self.total, 4)))
        