import numpy as np
import matplotlib.pyplot as plt

def tsp_plot(data, path_list, name):
    x = []
    y = []
    for val in path_list:
        coords = data.node_coords[val+1]
        x.append(coords[0])
        y.append(coords[1])

    plt.figure(figsize=(15,8))    
    plt.plot(x,y)
    plt.plot(x,y, 'ro')
    plt.text(x[0], y[0], 'Start')
    plt.text(x[-1], y[-1], 'End')
    x[0],y[0]
    plt.show()
    plt.savefig('./img/discrete/' + name)

def loss_plot(pop_size, fitness_list, name):
    arr = np.array([np.max(np.array(fitness_list[i:i+pop_size]) * -1) for i in range(0, len(fitness_list), pop_size)]) * -1
    plt.figure(figsize=(15,8))
    plt.xlabel('Iterations')
    plt.ylabel('Fitness')
    plt.plot(arr)
    plt.show()
    plt.savefig('./img/continuous/' + name)